# Helm Charts

## Start local repo

```bash
$ cd repo
$ docker compose up -d
```

## Build

```bash
$ helm package ./charts
```

## Publish

```bash
$ mv ministockmarket-charts-*.tgz ./repo/static
```

## Reindex

```bash
$ cd repo/static
$ helm repo index .
```